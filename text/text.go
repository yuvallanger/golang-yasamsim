// text
package text

import (
	//"fmt"
	"image/color"
	"strings"

	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/font/gofont/gobold"
	"golang.org/x/image/font/gofont/gomono"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/text"
)

var gomonofont *truetype.Font
var gomonoface font.Face
var goboldfont *truetype.Font
var goboldface font.Face

func init() {
	var err error

	gomonofont, err = truetype.Parse(gomono.TTF)
	if err != nil {
		panic(err)
	}
	opts := truetype.Options{
		//Hinting:    font.HintingFull,
		Hinting: font.HintingNone,
		//SubPixelsX: 64,
		//SubPixelsY: 64,
		SubPixelsX: 1,
		SubPixelsY: 1,
		DPI:        72,
		Size:       15,
	}
	gomonoface = truetype.NewFace(gomonofont, &opts)

	goboldfont, err = truetype.Parse(gobold.TTF)
	if err != nil {
		panic(err)
	}
	goboldface = truetype.NewFace(goboldfont, &opts)
}

func DrawTextLine(img *ebiten.Image, s string, x, y int, c color.Color) {
	text.Draw(img, s, goboldface, x, y, c)
}

func DrawMultilineText(img *ebiten.Image, s string, x, y, dy int, c color.Color) {
	lines := strings.Split(s, "\n")
	for i, line := range lines {
		ourY := y + dy*i
		DrawTextLine(img, line, x, ourY, c)
	}
}
