# TODOs!

## Mechanics

* TODO Make NPCs appear to the right of our hero, or just use a stage map.
* TODO Collision should use the object's depth, not the height of the sprite.
* THE WALLs
	* DONE Fix sticky wall collisions.
	* TODO Merge wall collision code into the entity types. (is that the correct wording?)
	* TODO Make enemies collide and respond to walls.
	* TODO Make projectiles collide and respond to walls.
* DONE Most of the time g.points was raised twice. Moving adding points to the ToBeDestroyed filtering stage fixed it.
    * TODO Grok why it happened
* TODO Add rudimentary life meter.
* TODO Add game over state.

## Art

* TODO Adjust or recreate sprites for a 16x24 pixels.
* TODO More NPC types needed.
