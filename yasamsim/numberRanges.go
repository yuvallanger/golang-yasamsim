package yasamsim

import (
	"math"
)

type float64Range struct {
	small, large float64
}

func (r float64Range) Small() float64 { return r.small }
func (r float64Range) Large() float64 { return r.large }

func (r float64Range) BetweenExclusive(x float64) bool {
	return r.Small() < x && x < r.Large()
}

// NewRange creates a float64Range out of two float64.
// It doesn't care about order, as it figures out which
// of the provided values is small and which is large for you.
//
// Fun fact: floats sounds like פלוץ.
func NewRange(small, large float64) float64Range {
	return float64Range{
		small: math.Min(small, large),
		large: math.Max(small, large),
	}
}

func (r0 float64Range) Intersect(r1 float64Range) bool {
	return r0.Large() > r1.Small() && r1.Large() > r0.Small()
}
