package yasamsim

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRange(t *testing.T) {
	cases := []struct {
		small, big         float64
		smallWant, bigWant float64
	}{
		{10, -10, -10, 10},
		{10, 5, 5, 10},
		{-100, 100, -100, 100},
		{100, 100, 100, 100},
		{-10, 10, -10, 10},
		{0, 0, 0, 0},
		{10, 10, 10, 10},
		{-10, -10, -10, -10},
	}

	for _, c := range cases {
		assert := assert.New(t)
		//		t.Logf("Testing range [%v, %v]", c.small, c.big)
		want := NewRange(c.smallWant, c.bigWant)
		got := NewRange(c.small, c.big)
		assert.Equal(got, want)
	}
}

func TestIntersect(t *testing.T) {
	assert := assert.New(t)
	cases := []struct {
		small0, big0, small1, big1 float64
		want                       bool
	}{
		{-1, 0, 0, 1, false},
		{0, 1, 0.5, 2, true},
		{0, 1, 1, 2, false},
		{0, 1, 1, 2, false},
		{0, 1, 1, 2, false},
		{0, 1, 1, 2, false},
	}
	for _, c := range cases {
		r0 := NewRange(c.small0, c.big0)
		r1 := NewRange(c.small1, c.big1)
		got := r0.Intersect(r1)
		assert.Equal(got, c.want)
		//		if got != c.want {
		//			t.Errorf("Intersect(%v, %v) == %v, want %v",
		//				c.r0, c.r1, got, c.want,
		//			)
		//		}
	}
}

func TestBetweenExclusive(t *testing.T) {
	assert := assert.New(t)
	cases := []struct {
		small, big float64
		x          float64
		want       bool
	}{
		{-1, 1, -10, false},
		{-1, 1, -1, false},
		{-1, 1, -0.5, true},
		{-1, 1, -1, false},
		{-1, 1, 1, false},
	}
	for _, c := range cases {
		r := NewRange(c.small, c.big)
		got := r.BetweenExclusive(c.x)
		assert.Equal(got, c.want)
	}
}
