package yasamsim

import (
	"fmt"
	"image/color"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"

	"gitgud.io/yuvallanger/golang-yasamsim/constants"
)

func newPlayer(x, y float64) *Player {
	/*
		maxBound := images["hero_north_0"].Bounds().Max
	*/

	player := Player{}

	player.img, _ = ebiten.NewImage(
		constants.PlayerWidth,
		constants.PlayerHeight,
		ebiten.FilterDefault,
	)

	player.faceX = faceWest

	player.SetLeftSide(x)
	player.SetUpperSide(y)

	return &player
}

func (c *Player) Step(g *game) {
	c.Fall()

	for i, wall := range g.walls {
		if wall.HorizontalRange().Intersect(c.HorizontalRange()) &&
			wall.VerticalRange().Intersect(c.VerticalRange()) {
			g.walls[i].collisionDebug = true
			if debug {
				fmt.Println(i, ":", wall.X(), wall.Y())
			}
		} else {
			g.walls[i].collisionDebug = false
		}
	}

	c.collisionWithWalls(g.walls)
	c.clipWithScreenBorders()

	if c.projectileCountdown > 0 {
		c.projectileCountdown -= 1
	}
}

func (p *Player) collisionWithWalls(walls []Wall) {
	bothMovement := Player(*p)
	horizontalMovement := Player(*p)
	verticalMovement := Player(*p)

	bothMovement.SetX(p.X() + p.DX())
	bothMovement.SetY(p.Y() + p.DY())

	horizontalMovement.SetX(p.X() + p.DX())
	verticalMovement.SetY(p.Y() + p.DY())

	bothCollision := false
	horizontalCollision := false
	verticalCollision := false

	for _, wall := range walls {
		if bothMovement.RectangleCollision(wall) {
			bothCollision = true
		}
		if horizontalMovement.RectangleCollision(wall) {
			horizontalCollision = true
		}
		if verticalMovement.RectangleCollision(wall) {
			verticalCollision = true
		}
		if bothCollision && horizontalCollision && verticalCollision {
			return
		}
	}

	if bothCollision {
		if horizontalCollision {
			p.SetY(verticalMovement.Y())
		} else {
			p.SetX(horizontalMovement.X())
		}
	} else {
		p.SetX(bothMovement.X())
		p.SetY(bothMovement.Y())
	}
}

func (p *Player) clipWithScreenBorders() {
	// Clip to screen borders
	if p.RightSide() > constants.ScreenWidth {
		p.SetRightSide(constants.ScreenWidth)
	}
	if p.LeftSide() < 0 {
		p.SetLeftSide(0)
	}
	if p.LowerSide() > constants.ScreenHeight {
		p.SetLowerSide(constants.ScreenHeight)
	}
	if p.UpperSide() < 0 {
		p.SetUpperSide(0)
	}
}
func (c Player) Draw(screen *ebiten.Image) error {
	imageOpts := &ebiten.DrawImageOptions{}

	/*
		var spriteDirectionName string
		if c.faceX == faceWest && c.faceY == faceUp {
			spriteDirectionName = "west"
		} else if c.faceX == faceEast && c.faceY == faceUp {
			spriteDirectionName = "east"
		} else if c.faceX == faceGreenwich && c.faceY == faceUp {
			spriteDirectionName = "north"
		} else if c.faceX == faceWest && c.faceY == faceHorizon {
			spriteDirectionName = "west"
		} else if c.faceX == faceEast && c.faceY == faceHorizon {
			spriteDirectionName = "east"
		} else if c.faceX == faceGreenwich && c.faceY == faceHorizon {
			spriteDirectionName = "east"
		} else if c.faceX == faceWest && c.faceY == faceDown {
			spriteDirectionName = "west"
		} else if c.faceX == faceEast && c.faceY == faceDown {
			spriteDirectionName = "east"
		} else if c.faceX == faceGreenwich && c.faceY == faceDown {
			spriteDirectionName = "south"
		}
		spriteName := fmt.Sprintf("%s_%s_%d", c.style, spriteDirectionName, 0)
		img := Images[spriteName]
	*/

	y := c.UpperSide()
	x := c.LeftSide()
	imageOpts.GeoM.Translate(x, y-c.z)

	img, err := ebiten.NewImage(
		constants.PlayerWidth,
		constants.PlayerHeight,
		ebiten.FilterDefault,
	)
	if err != nil {
		return err
	}
	img.Fill(color.RGBA{R: 0, G: 0, B: 0xff, A: 0xff})

	err = screen.DrawImage(img, imageOpts)
	return err
}

type Player struct {
	faceDirection
	spriteMoving
	destroyable
	projectileCountdown int
}

func (c *Player) HandleInput(g *game) {
	c.dx, c.dy = 0, 0
	if ebiten.IsKeyPressed(ebiten.KeyUp) {
		c.dy += -constants.MovementSpeed
	}
	if ebiten.IsKeyPressed(ebiten.KeyDown) {
		c.dy += constants.MovementSpeed
	}
	if ebiten.IsKeyPressed(ebiten.KeyLeft) {
		c.dx += -constants.MovementSpeed
	}
	if ebiten.IsKeyPressed(ebiten.KeyRight) {
		c.dx += constants.MovementSpeed
	}

	if c.z <= 0 {
		c.dz = 0
		if ebiten.IsKeyPressed(ebiten.KeySpace) {
			c.dz = constants.JumpSpeed
		}
	}

	if c.dx < 0 {
		c.faceX = faceWest
		/*
			} else if c.dx == 0 {
				c.faceX = faceGreenwich
		*/
	} else if c.dx > 0 {
		c.faceX = faceEast
	}

	if c.dy < 0 {
		c.faceY = faceUp
	} else if c.dy == 0 {
		c.faceY = faceHorizon
	} else if c.dy > 0 {
		c.faceY = faceDown
	}

	if ebiten.IsKeyPressed(ebiten.KeyC) && c.faceX != faceGreenwich && c.projectileCountdown <= 0 {
		_ = "breakpoint"
		var ourProjectileSpeed float64

		if c.faceX == faceEast {
			ourProjectileSpeed = constants.ProjectileSpeed
		} else if c.faceX == faceWest {
			ourProjectileSpeed = -constants.ProjectileSpeed
		}

		projectile := newProjectile(c.x, c.y, c.z, ourProjectileSpeed)

		g.projectiles = append(g.projectiles, projectile)
		c.projectileCountdown = constants.ProjectileLatency
	}

	if inpututil.IsKeyJustPressed(ebiten.KeyD) {
	}
}
