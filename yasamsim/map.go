package yasamsim

import (
	"fmt"
	"strings"

	"gitgud.io/yuvallanger/golang-yasamsim/constants"
)

var levels []string = []string{
	`####################
#..................#
#..................#
#.....#.....#......#
#..................#
.........#.@........
....................
.......##...........
....................
....................
#..................#
#..................#
#..................#
#..................#
####################`,
}

type stageCoordinate float64
type mapCoordinate int

func levelMapCoordinateToStageCoordinate(
	mapX, mapY mapCoordinate,
) (stageX, stageY stageCoordinate) {
	stageX = stageCoordinate(float64(mapX) * float64(constants.ScreenWidth) / float64(constants.MapWidth))
	stageY = stageCoordinate(float64(mapY) * float64(constants.ScreenHeight) / float64(constants.MapHeight))
	return
}

func LevelMapToPlayerStageCoordinate(level string) (stageX, stageY stageCoordinate, err error) {
	stageCoordinates := findRunesStagePosition(level, '@')
	if len(stageCoordinates) > 1 {
		return 0, 0, fmt.Errorf("More than one player positions")
	}
	if len(stageCoordinates) == 0 {
		return 0, 0, fmt.Errorf("No player position")
	}

	return stageCoordinates[0].x, stageCoordinates[0].y, nil
}

func findRunesStagePosition(level string, r rune) []struct{ x, y stageCoordinate } {
	coordinates := []struct{ x, y stageCoordinate }{}

	levelLines := strings.Split(level, "\n")

	for mapY, levelRow := range levelLines {
		for mapX, currentMapRune := range levelRow {
			if currentMapRune == r {
				coordinate := struct{ x, y stageCoordinate }{}

				stageX, stageY := levelMapCoordinateToStageCoordinate(
					mapCoordinate(mapX),
					mapCoordinate(mapY),
				)
				coordinate.x = stageX
				coordinate.y = stageY
				coordinates = append(
					coordinates,
					coordinate,
				)
			}
		}
	}
	return coordinates
}
func LevelMapToWalls(level string) []Wall {
	walls := []Wall{}
	for _, coordinate := range findRunesStagePosition(level, '#') {
		walls = append(
			walls,
			*newWall(
				float64(coordinate.x),
				float64(coordinate.y),
			),
		)
	}
	return walls
}
